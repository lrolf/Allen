velo_validator validation:
TrackChecker output                               :    169277/   220966  76.61% ghosts
01_velo                                           :     24016/    95852  25.06% ( 25.08%),       285 (  1.17%) clones, pur  80.42%, hit eff  77.99%
02_long                                           :     15783/    56923  27.73% ( 27.79%),       132 (  0.83%) clones, pur  80.03%, hit eff  78.49%
03_long_P>5GeV                                    :     12550/    39606  31.69% ( 31.76%),        91 (  0.72%) clones, pur  80.14%, hit eff  79.05%
04_long_strange                                   :       892/     3003  29.70% ( 28.89%),         8 (  0.89%) clones, pur  82.51%, hit eff  80.42%
05_long_strange_P>5GeV                            :       503/     1446  34.79% ( 34.88%),         4 (  0.79%) clones, pur  82.78%, hit eff  81.64%
06_long_fromB                                     :      1043/     4222  24.70% ( 23.00%),        10 (  0.95%) clones, pur  79.83%, hit eff  78.05%
07_long_fromB_P>5GeV                              :       915/     3458  26.46% ( 24.72%),         6 (  0.65%) clones, pur  79.84%, hit eff  78.57%
08_long_electrons                                 :       995/     4760  20.90% ( 21.43%),         7 (  0.70%) clones, pur  81.72%, hit eff  80.12%
09_long_fromB_electrons                           :        43/      234  18.38% ( 20.83%),         0 (  0.00%) clones, pur  78.38%, hit eff  77.23%
10_long_fromB_electrons_P>5GeV                    :        26/      153  16.99% ( 20.28%),         0 (  0.00%) clones, pur  78.40%, hit eff  78.45%


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.946 (  4728/  4996)
Isolated             :  0.983 (  2588/  2633)
Close                :  0.906 (  2140/  2363)
False rate           :  0.008 (    36/  4764)
Real false rate      :  0.008 (    36/  4764)
Clones               :  0.000 (     0/  4728)


veloUT_validator validation:
TrackChecker output                               :     43026/    55451  77.59% ghosts
01_velo                                           :     12132/    95852  12.66% ( 12.79%),        65 (  0.53%) clones, pur  85.42%, hit eff  96.06%
02_velo+UT                                        :     12108/    83670  14.47% ( 14.61%),        65 (  0.53%) clones, pur  85.43%, hit eff  96.06%
03_velo+UT_P>5GeV                                 :     11286/    47632  23.69% ( 23.85%),        59 (  0.52%) clones, pur  85.50%, hit eff  96.15%
04_velo+notLong                                   :      2103/    38929   5.40% (  5.47%),         6 (  0.28%) clones, pur  85.90%, hit eff  96.15%
05_velo+UT+notLong                                :      2083/    27299   7.63% (  7.71%),         6 (  0.29%) clones, pur  85.97%, hit eff  96.14%
06_velo+UT+notLong_P>5GeV                         :      1922/     8455  22.73% ( 23.11%),         4 (  0.21%) clones, pur  86.07%, hit eff  96.35%
07_long                                           :     10029/    56923  17.62% ( 17.82%),        59 (  0.58%) clones, pur  85.32%, hit eff  96.05%
08_long_P>5GeV                                    :      9368/    39606  23.65% ( 23.84%),        55 (  0.58%) clones, pur  85.38%, hit eff  96.12%
09_long_fromB                                     :       844/     4222  19.99% ( 19.16%),         5 (  0.59%) clones, pur  85.52%, hit eff  96.22%
10_long_fromB_P>5GeV                              :       813/     3458  23.51% ( 22.32%),         5 (  0.61%) clones, pur  85.52%, hit eff  96.38%
11_long_electrons                                 :       188/     4760   3.95% (  4.16%),         3 (  1.57%) clones, pur  86.88%, hit eff  94.84%
12_long_fromB_electrons                           :        19/      234   8.12% (  8.91%),         0 (  0.00%) clones, pur  84.14%, hit eff  97.19%
13_long_fromB_electrons_P>5GeV                    :        15/      153   9.80% ( 11.11%),         0 (  0.00%) clones, pur  84.78%, hit eff  96.44%


forward_validator validation:
TrackChecker output                               :     28404/    38464  73.85% ghosts
for P>3GeV,Pt>0.5GeV                              :     21421/    28494  75.18% ghosts
01_long                                           :      9584/    56923  16.84% ( 17.11%),        54 (  0.56%) clones, pur  91.59%, hit eff  98.98%
02_long_P>5GeV                                    :      9073/    39606  22.91% ( 23.16%),        51 (  0.56%) clones, pur  91.60%, hit eff  99.08%
03_long_strange                                   :       332/     3003  11.06% ( 10.10%),         3 (  0.90%) clones, pur  93.68%, hit eff  98.93%
04_long_strange_P>5GeV                            :       291/     1446  20.12% ( 19.53%),         3 (  1.02%) clones, pur  93.70%, hit eff  99.08%
05_long_fromB                                     :       818/     4222  19.37% ( 18.41%),         4 (  0.49%) clones, pur  91.72%, hit eff  99.10%
06_long_fromB_P>5GeV                              :       791/     3458  22.87% ( 21.58%),         4 (  0.50%) clones, pur  91.71%, hit eff  99.15%
07_long_electrons                                 :       160/     4760   3.36% (  3.53%),         2 (  1.23%) clones, pur  92.96%, hit eff  97.63%
08_long_electrons_P>5GeV                          :       142/     2658   5.34% (  5.88%),         1 (  0.70%) clones, pur  93.34%, hit eff  98.37%
09_long_fromB_electrons                           :        16/      234   6.84% (  7.56%),         0 (  0.00%) clones, pur  91.01%, hit eff  94.87%
10_long_fromB_electrons_P>5GeV                    :        12/      153   7.84% (  9.03%),         0 (  0.00%) clones, pur  91.59%, hit eff  96.63%


muon_validator validation:
Muon fraction in all MCPs:                                              11305/   829808   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    111/    11989   0.01% 
Correctly identified muons with isMuon:                                   101/      111  90.99% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                      26/       29  89.66% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:       775/    11878   6.52% 
Ghost tracks identified as muon with isMuon:                             2548/    28404   8.97% 


rate_validator validation:
Hlt1KsToPiPi:                   43/  1000, ( 1290.00 +/-   192.45) kHz
Hlt1TrackMVA:                  259/  1000, ( 7770.00 +/-   415.60) kHz
Hlt1TwoTrackMVA:               470/  1000, (14100.00 +/-   473.49) kHz
Hlt1TwoTrackKs:                  3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SingleHighPtMuon:            1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SingleHighPtMuonNoMuID:      2/  1000, (   60.00 +/-    42.38) kHz
Hlt1LowPtMuon:                 139/  1000, ( 4170.00 +/-   328.19) kHz
Hlt1D2KK:                       19/  1000, (  570.00 +/-   129.52) kHz
Hlt1D2KPi:                      35/  1000, ( 1050.00 +/-   174.35) kHz
Hlt1D2PiPi:                     27/  1000, (  810.00 +/-   153.77) kHz
Hlt1DiMuonHighMass:             23/  1000, (  690.00 +/-   142.21) kHz
Hlt1DiMuonLowMass:              31/  1000, (  930.00 +/-   164.42) kHz
Hlt1DiMuonSoft:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowPtDiMuon:                46/  1000, ( 1380.00 +/-   198.73) kHz
Hlt1TrackMuonMVA:               11/  1000, (  330.00 +/-    98.95) kHz
Hlt1TrackElectronMVA:           43/  1000, ( 1290.00 +/-   192.45) kHz
Hlt1SingleHighPtElectron:       17/  1000, (  510.00 +/-   122.64) kHz
Hlt1DisplacedDielectron:        12/  1000, (  360.00 +/-   103.30) kHz
Hlt1DisplacedLeptons:           38/  1000, ( 1140.00 +/-   181.38) kHz
Hlt1SingleHighEt:                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:            896/  1000, (26880.00 +/-   289.60) kHz
Hlt1NoBeam:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamOne:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamTwo:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BothBeams:                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINNoBias:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:              1000/  1000, (30000.00 +/-     0.00) kHz
Hlt1RICH1Alignment:              7/  1000, (  210.00 +/-    79.09) kHz
Hlt1RICH2Alignment:              1/  1000, (   30.00 +/-    29.98) kHz
Hlt1BeamGas:                     0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                    1000/  1000, (30000.00 +/-     0.00) kHz


selreport_validator validation:
                            Events  Candidates
Hlt1KsToPiPi:                   43          46
Hlt1TrackMVA:                  259         393
Hlt1TwoTrackMVA:               470        1486
Hlt1TwoTrackKs:                  3           3
Hlt1SingleHighPtMuon:            1           1
Hlt1SingleHighPtMuonNoMuID:      2           2
Hlt1LowPtMuon:                 139         161
Hlt1D2KK:                       19          22
Hlt1D2KPi:                      35          39
Hlt1D2PiPi:                     27          31
Hlt1DiMuonHighMass:             23          28
Hlt1DiMuonLowMass:              31          41
Hlt1DiMuonSoft:                  0           0
Hlt1LowPtDiMuon:                46          58
Hlt1TrackMuonMVA:               11          11
Hlt1TrackElectronMVA:           43          44
Hlt1SingleHighPtElectron:       17          17
Hlt1DisplacedDielectron:        12          17
Hlt1DisplacedLeptons:           38           0
Hlt1SingleHighEt:                0           0
Hlt1GECPassthrough:            896           0
Hlt1NoBeam:                      0           0
Hlt1BeamOne:                     0           0
Hlt1BeamTwo:                     0           0
Hlt1BothBeams:                   0           0
Hlt1VeloMicroBias:               0           0
Hlt1ODINLumi:                    0           0
Hlt1ODINNoBias:                  0           0
Hlt1Passthrough:              1000           0
Hlt1RICH1Alignment:              7           8
Hlt1RICH2Alignment:              1           1
Hlt1BeamGas:                     0           0

Total decisions: 3123
Total tracks:    2121
Total SVs:       1659
Total hits:      51779
Total stdinfo:   26727

