velo_validator validation:
TrackChecker output                               :        94/    13749   0.68% ghosts
01_velo                                           :      7660/     7805  98.14% ( 98.12%),       248 (  3.14%) clones, pur  99.60%, hit eff  95.77%
02_long                                           :      4466/     4511  99.00% ( 98.92%),       123 (  2.68%) clones, pur  99.74%, hit eff  96.55%
03_long_P>5GeV                                    :      2925/     2941  99.46% ( 99.35%),        70 (  2.34%) clones, pur  99.81%, hit eff  97.16%
04_long_strange                                   :       214/      218  98.17% ( 98.02%),         6 (  2.73%) clones, pur  99.58%, hit eff  95.93%
05_long_strange_P>5GeV                            :       113/      116  97.41% ( 96.88%),         1 (  0.88%) clones, pur  99.93%, hit eff  97.60%
08_long_electrons                                 :       583/      596  97.82% ( 98.16%),        23 (  3.80%) clones, pur  98.31%, hit eff  95.94%


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  1.025 (   727/   709)
Isolated             :  1.025 (   727/   709)
Close                :  0.000 (     0/     0)
False rate           :  0.010 (     7/   734)
Real false rate      :  0.010 (     7/   734)
Clones               :  0.001 (     1/   727)


veloUT_validator validation:
TrackChecker output                               :        61/     3362   1.81% ghosts
01_velo                                           :      3071/     7805  39.35% ( 38.19%),        50 (  1.60%) clones, pur  99.59%, hit eff  96.08%
02_velo+UT                                        :      3069/     6705  45.77% ( 44.15%),        50 (  1.60%) clones, pur  99.60%, hit eff  96.08%
03_velo+UT_P>5GeV                                 :      2389/     3693  64.69% ( 63.84%),        41 (  1.69%) clones, pur  99.67%, hit eff  96.41%
04_velo+notLong                                   :       625/     3294  18.97% ( 19.53%),        13 (  2.04%) clones, pur  99.40%, hit eff  95.61%
05_velo+UT+notLong                                :       623/     2209  28.20% ( 28.21%),        13 (  2.04%) clones, pur  99.44%, hit eff  95.60%
06_velo+UT+notLong_P>5GeV                         :       422/      755  55.89% ( 55.49%),        10 (  2.31%) clones, pur  99.56%, hit eff  97.19%
07_long                                           :      2446/     4511  54.22% ( 52.66%),        37 (  1.49%) clones, pur  99.64%, hit eff  96.20%
08_long_P>5GeV                                    :      1967/     2941  66.88% ( 65.57%),        31 (  1.55%) clones, pur  99.70%, hit eff  96.24%
11_long_electrons                                 :        58/      596   9.73% ( 10.64%),         1 (  1.69%) clones, pur  98.92%, hit eff  95.83%


forward_validator validation:
TrackChecker output                               :        19/     2476   0.77% ghosts
for P>3GeV,Pt>0.5GeV                              :         8/     1316   0.61% ghosts
01_long                                           :      2285/     4511  50.65% ( 49.25%),        35 (  1.51%) clones, pur  99.78%, hit eff  99.65%
02_long_P>5GeV                                    :      1907/     2941  64.84% ( 64.00%),        30 (  1.55%) clones, pur  99.82%, hit eff  99.74%
03_long_strange                                   :        65/      218  29.82% ( 29.95%),         0 (  0.00%) clones, pur  99.66%, hit eff  99.49%
04_long_strange_P>5GeV                            :        54/      116  46.55% ( 47.05%),         0 (  0.00%) clones, pur  99.95%, hit eff  99.83%
07_long_electrons                                 :        51/      596   8.56% (  9.21%),         1 (  1.92%) clones, pur  99.45%, hit eff  99.68%
08_long_electrons_P>5GeV                          :        48/      336  14.29% ( 14.03%),         1 (  2.04%) clones, pur  99.42%, hit eff  99.66%


muon_validator validation:
Muon fraction in all MCPs:                                                917/    68484   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                     23/     2883   0.01% 
Correctly identified muons with isMuon:                                    19/       23  82.61% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:        67/     2860   2.34% 
Ghost tracks identified as muon with isMuon:                                0/       19   0.00% 


rate_validator validation:
Hlt1KsToPiPi:                    1/  1000, (   30.00 +/-    29.98) kHz
Hlt1TrackMVA:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowPtMuon:                   9/  1000, (  270.00 +/-    89.59) kHz
Hlt1D2KK:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2PiPi:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonLowMass:               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LowPtDiMuon:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DisplacedDielectron:         0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DisplacedLeptons:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighEt:                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:           1000/  1000, (30000.00 +/-     0.00) kHz
Hlt1NoBeam:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamOne:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamTwo:                     0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BothBeams:                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:               1/  1000, (   30.00 +/-    29.98) kHz
Hlt1ODINLumi:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINNoBias:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:              1000/  1000, (30000.00 +/-     0.00) kHz
Hlt1RICH1Alignment:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                     0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                    1000/  1000, (30000.00 +/-     0.00) kHz


selreport_validator validation:
                            Events  Candidates
Hlt1KsToPiPi:                    1           1
Hlt1TrackMVA:                    0           0
Hlt1TwoTrackMVA:                 0           0
Hlt1TwoTrackKs:                  0           0
Hlt1SingleHighPtMuon:            0           0
Hlt1SingleHighPtMuonNoMuID:      0           0
Hlt1LowPtMuon:                   9          10
Hlt1D2KK:                        0           0
Hlt1D2KPi:                       0           0
Hlt1D2PiPi:                      0           0
Hlt1DiMuonHighMass:              0           0
Hlt1DiMuonLowMass:               0           0
Hlt1DiMuonSoft:                  0           0
Hlt1LowPtDiMuon:                 0           0
Hlt1TrackMuonMVA:                0           0
Hlt1TrackElectronMVA:            0           0
Hlt1SingleHighPtElectron:        0           0
Hlt1DisplacedDielectron:         0           0
Hlt1DisplacedLeptons:            0           0
Hlt1SingleHighEt:                0           0
Hlt1GECPassthrough:           1000           0
Hlt1NoBeam:                      0           0
Hlt1BeamOne:                     0           0
Hlt1BeamTwo:                     0           0
Hlt1BothBeams:                   0           0
Hlt1VeloMicroBias:               1           0
Hlt1ODINLumi:                    0           0
Hlt1ODINNoBias:                  0           0
Hlt1Passthrough:              1000           0
Hlt1RICH1Alignment:              0           0
Hlt1RICH2Alignment:              0           0
Hlt1BeamGas:                     0           0

Total decisions: 2011
Total tracks:    12
Total SVs:       1
Total hits:      305
Total stdinfo:   2111

