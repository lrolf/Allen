###############################################################################
# (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      #
###############################################################################

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  TARGET_BRANCH: master

  ALLEN_DATA: "/scratch/allen_data"
  LCG_VERSION: "LCG_101"
  LB_NIGHTLY_SLOT: lhcb-head
  BINARY_TAG: x86_64_v2-centos7-gcc11-opt
  NO_LBLOGIN: "1" # prevent lbdocker containers to start LbLogin/LbEnv


  PROFILE_DEVICE: "a5000"
  RUN_THROUGHPUT_OPTIONS_CUDAPROF: "-n 500 -m 500 -r 1 -t 1"
  RUN_THROUGHPUT_OPTIONS_CUDA: "-n 500 -m 500 -r 1000 -t 16"
  RUN_THROUGHPUT_OPTIONS_HIP: "-n 2800 --events-per-slice 2800 -m 2800 -t 10 -r 100"
  RUN_THROUGHPUT_OPTIONS_CPU: "-n 100 -m 100 -r 200"

  AVG_THROUGHPUT_DECREASE_THRESHOLD: "-2.5" # (%); fail throughput check if averaged throughput % change falls below -2.5%
  DEVICE_THROUGHPUT_DECREASE_THRESHOLD: "-7.5" # (%); fail throughput check if single device throughput % change falls below -10.0%

  OVERRIDE_CUDA_ARCH_FLAG: "-gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_75,code=sm_75 -gencode=arch=compute_86,code=sm_86"

stages:
  - check            # Ensures the CI environment is valid
  - build
  - run              # Build and run (throughput, efficiency, etc...)
  - test             # Runs various tests of the software
  - publish          # Publishes the results of the tests and runs in channels and grafana

  - manual trigger   # Blocks full pipeline from running in merge requests
  - build full
  - run full         # Build and run (full child pipelines)
  - test full        # Tests (full)
  - publish full     # Publishing (full)

check-env:
  stage: check
  except:
    - /.*/@lhcb/Allen
  script:
    - |
      echo "The Allen CI depends on custom GitLab runners and therefore tests"
      echo "running on forks will fail. Please create a branch in the main"
      echo "repository at https://gitlab.cern.ch/lhcb/Allen/"
    - exit 1


.active_branches: &active_branches
  only:
    refs:
      - master
      - web
      - schedules
      - merge_requests


check-copyright:
  <<: *active_branches

  stage: check
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/-/raw/master/LbDevTools/SourceTools.py?inline=False"
    - python lb-check-copyright --license=Apache-2.0 origin/${TARGET_BRANCH}
  needs: []

check-formatting:
  <<: *active_branches
  stage: check
  image: gitlab-registry.cern.ch/lhcb-docker/style-checker
  script:
    - |
      if [ ! -e .clang-format ] ; then
        curl -o .clang-format "https://gitlab.cern.ch/lhcb-parallelization/Allen/raw/master/.clang-format?inline=false"
        echo '.clang-format' >> .gitignore
        git add .gitignore
      fi

    - curl -o lb-format "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"

    - python lb-format --format-patch apply-formatting.patch origin/master
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 week
  needs: []
    
# On the master branch: compile docs, copy to public directory and deploy from there with gitlab pages
pages:
  only:
    refs:
      - master
  stage: publish
  image: gitlab-registry.cern.ch/lhcb-core/lbdocker/centos7-build:latest
  tags:
    - cvmfs
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    # use nightly build for python environment, needed for API reference of python selection functions
    # if today's nightly build does not exist, use yesterday's
    - nightly_installation_path="/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/latest/Allen/InstallArea/"$BINARY_TAG
    - lb-run --nightly lhcb-head/latest Allen/HEAD make -C doc linkcheck || reasons+='ERROR failed link check\n'
    - lb-run --nightly lhcb-head/latest Allen/HEAD make -C doc html || reasons+='ERROR failed html generation\n'
    - mv doc/_build/html public
    - if [ -n "$reasons" ]; then echo -e $reasons; exit 1; fi
  allow_failure:
    exit_codes: 77
  artifacts:
    paths:
      - public
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/pip

# on any branch except for master: compile docs, copy to test directory, don't deploy
test_pages:
  only:
    refs:
      - web
      - schedules
      - merge_requests
  stage: test
  image: gitlab-registry.cern.ch/lhcb-core/lbdocker/centos7-build:latest
  tags:
    - cvmfs
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    # use nightly build for python environment, needed for API reference of python selection functions
    # if today's nightly build does not exist, use yesterday's
    - nightly_installation_path="/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-head/latest/Allen/InstallArea/"$BINARY_TAG
    - lb-run --nightly lhcb-head/latest Allen/HEAD make -C doc linkcheck || reasons+='ERROR failed link check\n'
    - lb-run --nightly lhcb-head/latest Allen/HEAD make -C doc html || reasons+='ERROR failed html generation\n'
    - mv doc/_build/html test
    - if [ -n "$reasons" ]; then echo -e $reasons; exit 1; fi
  allow_failure:
    exit_codes: 77
  artifacts:
    paths:
      - test
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/pip

include: "scripts/ci/config/main.yaml"
